const msStart = Date.now();

const animation = {
    spin: {
        duration: 4000,
        delay: 500,
        radius: 800,
        degrees: -360,
    },

    bob: {
        startDelay: 2500,
        startDuration: 2000,
        radius: 40,
        period: 700,
        faceDragRadius: 18,
        faceDragDelay: 1,
    },

    faceSlide: {
        delay: 2000,
        duration: 2000,
    },
};

const easeInOutQuartic = getBoundedEasingFunction(generatePolynomialEasingFunction(4));
const easeOutQuartic = getBoundedEasingFunction(generatePolynomialEasingFunction(4, "out"));
function getBoundedEasingFunction(fn=t => t) {
    return t => t > 1
            ? fn(1)
            : t < 0
                    ? fn(0)
                    : fn(t);
}
function generatePolynomialEasingFunction(degree=1, accelerationMode="inout") {
    switch (accelerationMode) {
        case "in":
            return t => t**degree;

        case "out":
            return t => (-1)**(degree - 1) * (t - 1)**degree + 1;

        case "inout": 
            return t => t < .5 ? 2**(degree - 1) * t**degree : (-2)**(degree - 1) * (t - 1)**degree + 1;

        default:
            throw new RangeError(`${accelerationMode} is an invalid acceleration mode`);
    }
}

const main = document.querySelector("main");

const circle = document.querySelector(".circle");
const face = document.querySelector(".face");

let firstFrame = true;

function frame() {
    if (firstFrame) {
        main.style.visibility = "visible";
        firstFrame = false;
    }

    const msElapsed = Date.now() - msStart;

    // Spin in and start bobbing
    const spinEaseFactor = easeOutQuartic((msElapsed - animation.spin.delay) / animation.spin.duration);
    const spinRadius = animation.spin.radius * (1 - spinEaseFactor);
    
    const bobEaseFactor = easeInOutQuartic((msElapsed - animation.bob.startDelay) / animation.bob.startDuration);
    const bobRadius = animation.bob.radius * bobEaseFactor * Math.cos(msElapsed / animation.bob.period);
    
    circle.style.transform =
            `translateY(${bobRadius}px)
            rotate(${animation.spin.degrees * spinEaseFactor}deg)
            translateX(${spinRadius * spinEaseFactor}px)
            scaleX(${spinEaseFactor})`;
    circle.style.opacity = .5 * spinEaseFactor + .5;

    // Slide in the face
    const faceSlideEaseFactor = easeOutQuartic((msElapsed - animation.faceSlide.delay) / animation.faceSlide.duration);

    face.style.transform =
            `translateY(${animation.bob.faceDragRadius * (Math.cos(msElapsed / animation.bob.period - animation.bob.faceDragDelay))}px)
            translateY(${100 * -(faceSlideEaseFactor - 1)}%)
            scaleY(${faceSlideEaseFactor})`;

    requestAnimationFrame(frame);
}

requestAnimationFrame(frame);